package com.classpath.coursekart.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
class HelloController {

    @GetMapping("/me")
    public ResponseEntity<OAuth2AuthenticationToken> greetUser(OAuth2AuthenticationToken loggedinUser)  {
        return ResponseEntity.ok(loggedinUser);
    }

}